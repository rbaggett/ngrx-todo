import { async, TestBed } from '@angular/core/testing';
import { SharedDataAccessTodoListModule } from './shared-data-access-todo-list.module';

describe('SharedDataAccessTodoListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedDataAccessTodoListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SharedDataAccessTodoListModule).toBeDefined();
  });
});
