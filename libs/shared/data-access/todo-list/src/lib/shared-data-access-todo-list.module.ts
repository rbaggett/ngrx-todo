import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromTodoList from './+state/todo-list.reducer';
import { TodoListEffects } from './+state/todo-list.effects';
import { TodoListFacade } from './+state/todo-list.facade';
import { DataPersistence } from '@nrwl/angular';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromTodoList.TODOLIST_FEATURE_KEY,
      fromTodoList.reducer
    ),
    EffectsModule.forFeature([TodoListEffects])
  ],
  providers: [TodoListFacade, DataPersistence]
})
export class SharedDataAccessTodoListModule {}
