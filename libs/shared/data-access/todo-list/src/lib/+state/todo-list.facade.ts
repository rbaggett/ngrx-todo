import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromTodoList from './todo-list.reducer';
import * as TodoListSelectors from './todo-list.selectors';
import * as TodoListActions from './todo-list.actions';
import { TodoListEntity } from './todo-list.models';

@Injectable()
export class TodoListFacade {
  loaded$ = this.store.pipe(select(TodoListSelectors.getTodoListLoaded));
  allTodoList$ = this.store.pipe(select(TodoListSelectors.getAllTodoList));

  constructor(private store: Store<fromTodoList.TodoListPartialState>) {}

  loadAll() {
    this.store.dispatch(TodoListActions.loadTodoList());
  }

  addTodo(todo: TodoListEntity) {
    this.store.dispatch(TodoListActions.addTodo({ todo }));
  }

  removeTodo(todo: TodoListEntity) {
    this.store.dispatch(TodoListActions.removeTodo({ todo }));
  }
}
