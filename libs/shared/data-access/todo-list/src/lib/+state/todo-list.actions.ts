import { createAction, props } from '@ngrx/store';
import { TodoListEntity } from './todo-list.models';

export enum TodoListActionTypes {
  LoadTodoList = '[TodoList] Load TodoList',
  LoadTodoListSuccess = '[TodoList] Load TodoList Success',
  LoadTodoListFailure = '[TodoList] Load TodoList Failure',
  LoadTodoListAdd = '[TodoList] Add Todo',
  LoadTodoListAddSuccess = '[TodoList] Add Todo Success',
  LoadTodoListAddFailure = '[TodoList] Add Todo Failure',
  LoadTodoListRemove = '[TodoList] Remove Todo',
  LoadTodoListRemoveSuccess = '[TodoList] Remove Todo Success',
  LoadTodoListRemoveFailure = '[TodoList] Remove Todo Failure'
}

export const loadTodoList = createAction(TodoListActionTypes.LoadTodoList);

export const loadTodoListSuccess = createAction(
  TodoListActionTypes.LoadTodoListSuccess,
  props<{ todoList: TodoListEntity[] }>()
);

export const loadTodoListFailure = createAction(
  TodoListActionTypes.LoadTodoListFailure,
  props<{ error: any }>()
);

export const addTodo = createAction(
  TodoListActionTypes.LoadTodoListAdd,
  props<{ todo: TodoListEntity }>()
);

export const addTodoSuccess = createAction(
  TodoListActionTypes.LoadTodoListAddSuccess,
  props<{ todo: TodoListEntity }>()
);

export const removeTodo = createAction(
  TodoListActionTypes.LoadTodoListRemove,
  props<{ todo: TodoListEntity }>()
);

export const removeTodoSuccess = createAction(
  TodoListActionTypes.LoadTodoListRemoveSuccess,
  props<{ todo: TodoListEntity }>()
);
