/**
 * Interface for the 'TodoList' data
 */
export interface TodoListEntity {
  id?: number;
  checked?: boolean;
  todo?: string;
}
