import { Injectable } from '@angular/core';
import { createEffect, Actions, Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { TodoListPartialState } from './todo-list.reducer';
import * as TodoListActions from './todo-list.actions';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TodoListEntity } from './todo-list.models';

const api = 'http://localhost:3334/todos-api';

@Injectable()
export class TodoListEffects {
  @Effect() loadTodoList = this.dataPersistence.fetch(
    TodoListActions.loadTodoList,
    {
      run: () => {
        return this.httpClient.get(`${api}/todos`).pipe(
          map(resp =>
            TodoListActions.loadTodoListSuccess({
              todoList: resp as TodoListEntity[]
            })
          )
        );
      },
      onError: error => TodoListActions.loadTodoListFailure({ error })
    }
  );

  @Effect() addTodo = this.dataPersistence.optimisticUpdate(
    TodoListActions.addTodo,
    {
      run: (
        action: ReturnType<typeof TodoListActions.addTodo>,
        state: TodoListPartialState
      ) => {
        return this.httpClient.post(`${api}/addTodo`, action).pipe(
          map(resp =>
            TodoListActions.addTodoSuccess({
              todo: resp as TodoListEntity
            })
          )
        );
      },
      undoAction: (
        action: ReturnType<typeof TodoListActions.addTodo>,
        e: any
      ) => {
        return {
          type: 'UNDO_ADD_TODO',
          payload: action
        };
      }
    }
  );

  @Effect() removeTodo = this.dataPersistence.optimisticUpdate(
    TodoListActions.removeTodo,
    {
      run: (
        action: ReturnType<typeof TodoListActions.removeTodo>,
        state: TodoListPartialState
      ) => {
        return this.httpClient.post(`${api}/removeTodo`, action).pipe(
          map(resp =>
            TodoListActions.removeTodoSuccess({
              todo: resp as TodoListEntity
            })
          )
        );
      },
      undoAction: (
        action: ReturnType<typeof TodoListActions.removeTodo>,
        e: any
      ) => {
        return {
          type: 'UNDO_REMOVE_TODO',
          payload: action
        };
      }
    }
  );

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<TodoListPartialState>
  ) {}
}
