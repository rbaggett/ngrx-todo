import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  TODOLIST_FEATURE_KEY,
  TodoListState,
  TodoListPartialState,
  todoListAdapter
} from './todo-list.reducer';

// Lookup the 'TodoList' feature state managed by NgRx
export const getTodoListState = createFeatureSelector<
  TodoListPartialState,
  TodoListState
>(TODOLIST_FEATURE_KEY);

const { selectAll, selectEntities } = todoListAdapter.getSelectors();

export const getTodoListLoaded = createSelector(
  getTodoListState,
  (state: TodoListState) => state.loaded
);

export const getTodoListError = createSelector(
  getTodoListState,
  (state: TodoListState) => state.error
);

export const getAllTodoList = createSelector(
  getTodoListState,
  (state: TodoListState) => selectAll(state)
);

export const getTodoListEntities = createSelector(
  getTodoListState,
  (state: TodoListState) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getTodoListState,
  (state: TodoListState) => state.selectedId
);

export const getSelected = createSelector(
  getTodoListEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);
