import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import * as TodoListActions from './todo-list.actions';
import { TodoListEntity } from './todo-list.models';
export const TODOLIST_FEATURE_KEY = 'todoList';

export interface TodoListState extends EntityState<TodoListEntity> {
  selectedId?: string | number; // which TodoList record has been selected
  loaded: boolean; // has the TodoList list been loaded
  error?: string | null; // last none error (if any)
}

export interface TodoListPartialState {
  readonly [TODOLIST_FEATURE_KEY]: TodoListState;
}

export const todoListAdapter: EntityAdapter<
  TodoListEntity
> = createEntityAdapter<TodoListEntity>({
  selectId: (todoListEntity: TodoListEntity) => todoListEntity.id
});

export const initialState: TodoListState = todoListAdapter.getInitialState({
  loaded: false
});

const todoListReducer = createReducer(
  initialState,
  on(TodoListActions.loadTodoList, state => {
    return {
      ...state,
      loaded: false,
      error: null
    };
  }),
  on(TodoListActions.loadTodoListSuccess, (state, { todoList }) => {
    return todoListAdapter.addAll(todoList, { ...state, loaded: true });
  }),
  on(TodoListActions.loadTodoListFailure, (state, { error }) => ({
    ...state,
    error
  })),
  on(TodoListActions.addTodoSuccess, (state, { todo }) => {
    return todoListAdapter.addOne(todo, state);
  }),
  on(TodoListActions.removeTodoSuccess, (state, { todo }) => {
    return todoListAdapter.removeOne(todo.id, state);
  })
);

export function reducer(state: TodoListState | undefined, action: Action) {
  return todoListReducer(state, action);
}
