module.exports = {
  name: 'shared-data-access-todo-list',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/shared/data-access/todo-list',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
