import { Injectable } from '@nestjs/common';
import { TodoListEntity } from '@nx-todos/shared/data-access/todo-list';

@Injectable()
export class AppService {
  id: number = 0;
  todos: TodoListEntity[] = [{ id: 0, todo: `Default Todo` }];

  getData(): TodoListEntity[] {
    return this.todos;
  }

  addTodo({ todo }) {
    this.id += 1;
    const newTodo = { id: this.id, todo: todo.todo.todo };
    this.todos.push(newTodo);
    return newTodo;
  }

  removeTodo({ todo }) {
    this.todos = this.todos.filter(t => t.id !== todo.id);
    return todo;
  }
}
