import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('todos')
  getData() {
    return this.appService.getData();
  }

  @Post('addTodo')
  addTodo(@Body() todo: any) {
    return this.appService.addTodo(todo);
  }

  @Post('removeTodo')
  removeTodo(@Body() todo: any) {
    return this.appService.removeTodo(todo);
  }
}