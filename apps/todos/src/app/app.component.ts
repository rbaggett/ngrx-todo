import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
  TodoListFacade,
  TodoListEntity
} from '@nx-todos/shared/data-access/todo-list';

@Component({
  selector: 'nx-todos-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  todos$ = this.todoList.allTodoList$;
  displayedColumns: string[] = ['select', 'id', 'todo', 'action'];
  todoForm;

  constructor(
    private todoList: TodoListFacade,
    private formBuilder: FormBuilder
  ) {
    this.todoForm = this.formBuilder.group({ todo: '' });
  }

  ngOnInit() {
    this.todoList.loadAll();
  }

  onSubmit(todo: string) {
    this.todoList.addTodo({ todo });
  }

  removeTodo(todo: TodoListEntity) {
    this.todoList.removeTodo(todo);
    console.log(todo);
  }
}
